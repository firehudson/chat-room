var express = require('express');
var router = express.Router();
const axios = require('axios');

module.exports = app => {

  /* GET users listing. */
  router.get('/:id', async function(req, res, next) {
    const response = await axios('https://api.wahiride.com/api/wrauth/profile/' + req.params.id);

    res.send(response.data);
  });

  return router;
}
