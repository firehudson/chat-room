
const Sequelize = require('sequelize');
const config = require('config');



const sequelizeParams = process.env.DATABASE_URL ?
  [process.env.DATABASE_URL] :
  [config.db.database, config.db.username, config.db.password, {
    host: config.db.host,
    dialect: config.db.dialect,

    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000
    },
  }];

const sequelize = new Sequelize(...sequelizeParams);

module.exports = app => new Promise((resolve, reject) => {
  sequelize
    .authenticate()
    .then(() => {
      console.log('Connection has been established successfully.');
      app.sequelize = sequelize;
      resolve();
    })
    .catch(err => {
      console.error('Unable to connect to the database:', err);
      reject();
    });
});