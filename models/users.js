'use strict';
module.exports = (sequelize, DataTypes) => {
  var tbl_user = sequelize.define('users', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    email: DataTypes.STRING,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  }, {});
  tbl_user.associate = function(models) {
    // associations can be defined here
  };
  return tbl_user;
};