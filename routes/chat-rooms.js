var express = require('express');
var router = express.Router();
const axios = require('axios');

module.exports = app => {
  const chatRoom = app.db['chat_rooms'];
  const messages = app.db['messages'];

  router.get('/', async function(req, res, next) {
    const { driver_id, passenger_id, trip_id } = req.query;

    try {
      if (!driver_id || !passenger_id || !trip_id) {
        throw new Error('trip_id, driver_id and passenger_id are required');
      } else if (driver_id === passenger_id) {
        throw new Error('driver_id and passenger_id can not be same');
      } else {
        const chats = await chatRoom.findOrCreate({
          where: req.query,
          defaults: {
            messages: [],
          }
        })

        res.send(chats);
      }
    } catch(e) {
      console.error(e)
      res.status(400).send(e.message); 
    }
  });

  router.get('/messages', async function(req, res, next) {
    try {
      if (!Array.isArray(JSON.parse(req.query.ids))) {
        throw new Error('ids should be an array');
      }

      res.status(200).send({
        messages: await messages.findAll({ where: { id: JSON.parse(req.query.ids) } }),
      });
    } catch(e) {
      console.error(e)
      res.status(400).send(e.message); 
    }
  });

  router.post('/send-message', async function(req, res, next) {
    try {
      const { senderId, message, chatRoomId } = req.body;
      if (!senderId || !message, !chatRoomId) {
        throw new Error('senderId, chatRoomId and message are required');
      }

      let chat = await chatRoom.findOne({
        where: {
          $and: { id: chatRoomId },
        }
      })

      if (!chat) {
        throw new Error(`No chat room with id ${chatRoomId} present!`);
      }
      console.log(chat.driver_id)
      if (chat.driver_id !== senderId && chat.passenger_id !== senderId) {
        throw new Error(`You are not a participant of this conversation!`);
      }

      const Message = await messages.create({
        sender: senderId,
        message: message,
      });

      chat.messages.push(Message.id);
      chat = await chat.update({ messages: chat.messages })

      const response = {
        chat,
        messages: await messages.findAll({ where: { id: chat.messages } })
      };

      res.status(200).send(response)
    } catch(e) {
      console.error(e)
      res.status(400).send(e.message); 
    }
  })

  return router;
}
