const dbConfig = require('./database');

module.exports = Object.assign({}, {
  test: 'test',
}, {
  db: dbConfig[process.env.NODE_ENV]
});