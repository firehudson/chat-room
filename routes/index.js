var express = require('express');
var router = express.Router();
const app = express();

module.exports = app => {
  /* GET home page. */
  router.get('/', (req, res, next) => {
    res.status(200)
      .send(`Wahi Chat Room running at ${process.env.PORT || '3000'}`);
  });
  
  return router;
};
  