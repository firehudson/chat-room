'use strict';
module.exports = (sequelize, DataTypes) => {
  var chat_room = sequelize.define('chat_rooms', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    driver_id: DataTypes.INTEGER,
    passenger_id: DataTypes.INTEGER,
    trip_id: DataTypes.INTEGER,
    messages: DataTypes.JSON,
  }, {});
  chat_room.associate = function(models) {
    // associations can be defined here
  };
  return chat_room;
};